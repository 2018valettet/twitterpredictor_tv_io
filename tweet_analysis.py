import matplotlib.pyplot as plt
import numpy as np

def extract_most_retweeted(data):
    rt_max  = np.max(data['RTs'])
    rt  = data[data.RTs == rt_max].index[0]

    # Max RTs:
    print("The tweet with more retweets is: \n{}".format(data['tweet_textual_content'][rt]))
    print("Number of retweets: {}".format(rt_max))
    print("{} characters.\n".format(data['len'][rt]))

###extract_most_liked reprend le principe de la fonction ci-dessus, mais pour le tweet le plus liké
def extract_most_liked(data):
    likes_max  = np.max(data['Likes'])
    likes = data[data.Likes == likes_max].index[0]

    # Max Likes:
    print("The tweet with more likes is: \n{}".format(data['tweet_textual_content'][likes]))
    print("Number of retweets: {}".format(likes_max))
    print("{} characters.\n".format(data['len'][likes]))

###extract_most_efficient extrait le tweet le plus efficace, c'est-à-dire le plus retweeté par rapport à sa longueur
def extract_most_efficient(data):
    efficiency = []
    for i in range(len(data)):
        efficiency.append([[data['tweet_textual_content']],[data['RTs'][i]/data['len'][i]]])
    np_efficiency = np.array(efficiency)
    efficiency_max = np.max(efficiency)
    a=str(efficiency_max)
    most_efficient = data[(data['RTs']/data['len']) == efficiency_max].index[0]

    # Most efficient tweet:
    print("The most efficient tweet is: " + data(['tweet_textual_content'][most_efficient]))
    print("Its efficiency is: "+a+"RTs/number of characters")

def follow_evolution(data):
    tfav = pd.Series(data=data['Likes'].values, index=data['Date'])
    tret = pd.Series(data=data['RTs'].values, index=data['Date'])

    # Likes vs retweets visualization:
    tfav.plot(figsize=(16,4), label="Likes", legend=True)
    tret.plot(figsize=(16,4), label="Retweets", legend=True)

    plt.show()

