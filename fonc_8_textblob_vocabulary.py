from textblob import TextBlob
from textblob import Word

def extract_vocabulary(data,k):
    #tweet_collection est une liste contenant des tweets sous forme de chaînes de caractères
    #k est le seuil de redondance des mots à partir duquel on les supprime du vocabulaire
    vocabulary=[]
    complete_chain=""
    for tweet in data["tweet_textual_content"]:
        complete_chain+=" "
        complete_chain+=tweet ###On colle d'abord les tweets bout à bout
    T=TextBlob(complete_chain)
    T.correct()   ### Dans le cas où les tweets contiendraient des fautes d'orthographe
    word_list=T.words
    for word in word_list:
        word=word.lemmatize()
        if word_list.word_counts[word]>k:
            if word not in vocabulary:
                vocabulary.append(word)
        if word_list.word_counts[word]<=k:
            vocabulary.remove(word)   ###On supprime les mots trop redondants
    return vocabulary




